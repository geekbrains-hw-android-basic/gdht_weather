package albul.basic.android.ru.gdht_weather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailsWeatherListener} interface
 * to handle interaction events.
 * Use the {@link DetailsWeatherFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsWeatherFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Boolean humidityChecked;

    private TextView temperatureValue;
    private TextView cityValue;
    private ImageView weatherImage;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private DetailsWeatherListener mListener;

    public DetailsWeatherFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsWeatherFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsWeatherFragment newInstance(String param1, String param2) {
        DetailsWeatherFragment fragment = new DetailsWeatherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_weather, container, false);
        temperatureValue = view.findViewById(R.id.activity_main_temperature_value);
        cityValue = view.findViewById(R.id.activity_main_current_city);
        weatherImage = view.findViewById(R.id.weather_image);
        weatherImage.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.clickOnDetails();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.clickOnDetails();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailsWeatherListener) {
            mListener = (DetailsWeatherListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DetailsWeatherListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void changeDetails(Intent intent) {
        String city = intent.getStringExtra(MainActivity.CITY);
        humidityChecked = intent.getBooleanExtra(MainActivity.KEY_HUMIDITY_CHECKED, false);
        updateTemperatureText();
        updateCityText(city);
        updateWeatherImage();
    }

    private void updateWeatherImage() {
        int rnd = (int) (Math.random() * 3 + 1);
        switch (rnd) {
            case 1:
                weatherImage.setImageResource(R.drawable.sun_weather);
                break;
            case 2:
                weatherImage.setImageResource(R.drawable.clouds_weather);
                break;
            case 3:
                weatherImage.setImageResource(R.drawable.clouds_sun_weather);
                break;
            default:
                break;
        }
    }

    private void updateTemperatureText() {
        String text = ActivityDataSaver.getTemperature() + "\u00B0" + "C";
        if (humidityChecked) {
            text += "\nHUM: 10%";
        }
        temperatureValue.setText(text);
    }

    private void updateCityText(String city) {
        cityValue.setText(city);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface DetailsWeatherListener {
        // TODO: Update argument type and name
        void clickOnDetails();
    }
}
