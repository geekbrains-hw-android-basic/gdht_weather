package albul.basic.android.ru.gdht_weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class SettingActivity extends AppCompatActivity {

    public static final String RESULT_KEY_CITY = "RESULT_KEY_CITY";
    public static final String RESULT_KEY_HUMIDITY = "HUMIDITY";
    private TextView textCity;
    private EditText editorCity;
    private Button btnApply;
    private CheckBox humidity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initUI();
    }

    private void initUI() {
        editorCity = findViewById(R.id.activity_setting_editor_city);
        btnApply = findViewById(R.id.activity_setting_btn_apply);
        humidity = findViewById(R.id.activity_setting_checkbox_humidity);
        btnApply.setOnClickListener((view) -> {
            Intent intent = new Intent();
            intent.putExtra(RESULT_KEY_CITY, editorCity.getText().toString());
            intent.putExtra(RESULT_KEY_HUMIDITY, humidity.isChecked());
            setResult(200, intent);
            finish();
        });
        Intent intent = getIntent();
        if (intent != null) {
            editorCity.setText(intent.getStringExtra(MainActivity.CITY));
            humidity.setChecked(intent.getBooleanExtra(MainActivity.KEY_HUMIDITY_CHECKED, false));
        }
    }


}
