package albul.basic.android.ru.gdht_weather;

public final class ActivityDataSaver {

    private static ActivityDataSaver instance = null;

    private static int temperature;
    private static String cityText;
    public static boolean humidityChecked = false;

    private ActivityDataSaver(int temperature) {
        this.temperature = temperature;
    }

    public static String getCity() {
        return cityText;
    }

    public static void setCity(String cityText) {
        ActivityDataSaver.cityText = cityText;
    }

    public static int getTemperature() {
        temperature = (int) (Math.random() * 100 - 50);
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
        setCity("Yoshkar-Ola");
    }

    public static ActivityDataSaver getInstance(int temperature) {
        if (instance == null) {
            instance = new ActivityDataSaver(temperature);
        }
        return instance;
    }

}
