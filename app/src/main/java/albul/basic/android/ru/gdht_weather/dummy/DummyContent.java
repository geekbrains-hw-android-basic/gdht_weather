package albul.basic.android.ru.gdht_weather.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<OneCity> ITEMS = new ArrayList<OneCity>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, OneCity> ITEM_MAP = new HashMap<String, OneCity>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        addItem(createOneCity(ITEMS.size(), "Йошкар-Ола"));
        addItem(createOneCity(ITEMS.size(), "Сывтыкар"));
        addItem(createOneCity(ITEMS.size(), "Караганда"));
        addItem(createOneCity(ITEMS.size(), "Москва"));
    }

    private static void addItem(OneCity item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static OneCity createOneCity(int id, String cityName) {
        return new OneCity(String.valueOf(id), cityName);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class OneCity {
        public final String id;
        public final String content;

        public OneCity(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return "OneCity{" +
                    "id='" + id + '\'' +
                    ", content='" + content + '}';
        }
    }
}
