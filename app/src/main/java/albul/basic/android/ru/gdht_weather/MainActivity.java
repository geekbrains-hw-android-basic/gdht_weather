package albul.basic.android.ru.gdht_weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import albul.basic.android.ru.gdht_weather.dummy.DummyContent;

public class MainActivity extends AppCompatActivity
        implements CityNameFragment.OnListFragmentInteractionListener, DetailsWeatherFragment.DetailsWeatherListener {

    public static final String CITY = "CITY";
    public static final String KEY_HUMIDITY_CHECKED = "HUMIDITY";
    ActivityDataSaver activityDataSaver;
    private Button btnSet;
    private TextView temperatureValue;
    private TextView cityValue;
    private ImageView weatherImage;
    CityNameFragment cityNameFragment;
    DetailsWeatherFragment detailsWeatherFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityDataSaver = ActivityDataSaver.getInstance(ActivityDataSaver.getTemperature());
        initUI();
    }

    private void fragmentInit() {
        cityNameFragment = (CityNameFragment) getSupportFragmentManager().findFragmentById(R.id.city_name_fragment);
        detailsWeatherFragment = (DetailsWeatherFragment) getSupportFragmentManager().findFragmentById(R.id.details_weather_fragment);
    }

    private int getTemperature() {
        int temp = ActivityDataSaver.getTemperature();
        updateTemperatureText();
        return temp;
    }

    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activityDataSaver.setCity("Йошкар-Ола");

        btnSet = findViewById(R.id.activity_main_btn_setting);
        temperatureValue = findViewById(R.id.activity_main_temperature_value);
        cityValue = findViewById(R.id.activity_main_current_city);
        weatherImage = findViewById(R.id.weather_image);

        btnSet.setOnClickListener((view) -> {
            //startWeatherHistoryActivity();
            startSettingActivity();
        });
        fragmentInit();
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();*/
//                startSettingActivity();
//            }
//        });


        //CityNameFragment cityNameFragment = new CityNameFragment();
        // getFragmentManager().beginTransaction().add(R.id.frame_layout, cityNameFragment).commit();

    }

    private void updateWeatherImage() {
        if (detailsWeatherFragment != null && detailsWeatherFragment.isVisible()) {
            int rnd = (int) (Math.random() * 3 + 1);
            switch (rnd) {
                case 1:
                    weatherImage.setImageResource(R.drawable.sun_weather);
                    break;
                case 2:
                    weatherImage.setImageResource(R.drawable.clouds_weather);
                    break;
                case 3:
                    weatherImage.setImageResource(R.drawable.clouds_sun_weather);
                    break;
                default:
                    break;
            }
        }
    }

    private void updateTemperatureText() {
        if (detailsWeatherFragment != null && detailsWeatherFragment.isVisible()) {
            String text = activityDataSaver.getTemperature() + "\u00B0" + "C";
            if (ActivityDataSaver.humidityChecked) {
                text += "\nHUM: 10%";
            }
            temperatureValue.setText(text);
        }
    }

    private void updateCityText(String city) {
        ActivityDataSaver.setCity(city);
        cityValue.setText(city);
    }

    private void startSettingActivity() {
        Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
        intent.putExtra(CITY, ActivityDataSaver.getCity());
        intent.putExtra(KEY_HUMIDITY_CHECKED, ActivityDataSaver.humidityChecked);
        startActivityForResult(intent, 200);
    }

    private void startWeatherHistoryActivity() {
        Intent intent = new Intent(getApplicationContext(), WeatherHistoryActivity.class);
        intent.putExtra(CITY, ActivityDataSaver.getCity());
        startActivityForResult(intent, 200);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (detailsWeatherFragment != null && detailsWeatherFragment.isVisible()) {
            updateTemperatureText();
            updateCityText(activityDataSaver.getCity());
            updateWeatherImage();
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            activityDataSaver.setCity(data.getStringExtra(SettingActivity.RESULT_KEY_CITY));
            activityDataSaver.humidityChecked = data.getBooleanExtra(SettingActivity.RESULT_KEY_HUMIDITY, false);
        }
    }

    @Override
    public void onListFragmentInteraction(DummyContent.OneCity item) {
        if (detailsWeatherFragment != null && detailsWeatherFragment.isVisible()) {
            updateTemperatureText();
            updateCityText(item.content);
            updateWeatherImage();
        } else {
            Intent intent = new Intent(getApplicationContext(), DetailsWeatherActivity.class);
            intent.putExtra(CITY, item.content);
            intent.putExtra(KEY_HUMIDITY_CHECKED, ActivityDataSaver.humidityChecked);
            startActivity(intent);

        }
    }

    @Override
    public void clickOnDetails() {

    }
}
