package albul.basic.android.ru.gdht_weather;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import albul.basic.android.ru.gdht_weather.CityNameFragment.OnListFragmentInteractionListener;
import albul.basic.android.ru.gdht_weather.dummy.DummyContent.OneCity;

/**
 * {@link RecyclerView.Adapter} that can display a {@link OneCity} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCityNameRecyclerViewAdapter extends RecyclerView.Adapter<MyCityNameRecyclerViewAdapter.ViewHolder> {

    private final List<OneCity> mValues;
    private final OnListFragmentInteractionListener mListener;

    MyCityNameRecyclerViewAdapter(List<OneCity> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_cityname, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        // holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);
        updateWeatherImage(holder.mImageView);
        updateTemperatureText(holder.mTemperatureView);

        holder.mView.setOnClickListener((View v) -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });
    }
    

    private void updateWeatherImage(ImageView imageView) {
        int rnd = (int) (Math.random() * 3 + 1);
        switch (rnd) {
            case 1:
                imageView.setImageResource(R.drawable.sun_weather);
                break;
            case 2:
                imageView.setImageResource(R.drawable.clouds_weather);
                break;
            case 3:
                imageView.setImageResource(R.drawable.clouds_sun_weather);
                break;
            default:
                break;
        }
    }

    private void updateTemperatureText(TextView temperatureValue) {
        String text = ActivityDataSaver.getTemperature() + "\u00B0" + "C";
        if (ActivityDataSaver.humidityChecked) {
            text += "\nHUM: 10%";
        }
        temperatureValue.setText(text);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        //private final TextView mIdView;
        private final TextView mContentView;
        private final ImageView mImageView;
        private final TextView mTemperatureView;
        private OneCity mItem;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.content);
            mImageView = (ImageView) view.findViewById(R.id.weather_image);
            mTemperatureView = (TextView) view.findViewById(R.id.activity_main_temperature_value);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
