package albul.basic.android.ru.gdht_weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class DetailsWeatherActivity extends AppCompatActivity implements DetailsWeatherFragment.DetailsWeatherListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_weather);
        Intent intent = getIntent();
        DetailsWeatherFragment fragment = (DetailsWeatherFragment) getSupportFragmentManager().findFragmentById(R.id.details_weather_fragment);
        fragment.changeDetails(intent);
    }

    @Override
    public void clickOnDetails() {
        finish();
    }
}
